from django.db import models

# Create your models here.
from users.models import User


class Loan_application(models.Model):
    description_loan = models.CharField(max_length=100)
    value=models.FloatField()
    fee_quantity=models.IntegerField()
    interest=models.FloatField()
    date_application=models.DateField(auto_now=True)
    status_application=models.CharField(default='En proceso..', max_length=100)
    id_user=models.ForeignKey(User,limit_choices_to = {'role_id': 2},on_delete=models.CASCADE)

    def __str__(self):
        return self.description_loan


class Loan(models.Model):
    id_cobrador = models.ForeignKey(User,limit_choices_to = {'role_id': 3}, on_delete=models.CASCADE)
    id_request = models.ForeignKey(Loan_application, on_delete=models.CASCADE)
    comment=models.TextField()
    balance = models.FloatField()
    is_completed=models.BooleanField(default=False)

    def __str__(self):
        return f'{self.id_request}'