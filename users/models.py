from django.db import models
from django.db.models import Q

# Create your models here
class Role(models.Model):
    description_role = models.CharField(max_length=200)
    def __str__(self):
        return self.description_role


class User(models.Model):
    name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    card_number=models.IntegerField()
    address = models.CharField(max_length=200)
    phone = models.IntegerField()
    email = models.EmailField()
    role = models.ForeignKey(Role,limit_choices_to=Q(id=2) | Q(id=3),on_delete= models.CASCADE)

    def __str__(self):
        return self.name + self.last_name



